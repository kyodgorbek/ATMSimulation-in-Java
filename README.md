# ATMSimulation-in-Java



import java.swing.JFrame;

public class ATMSimulation {
  
  public static void main(String[] args){
   {
     if (args.legth == 0)
     {
        System.out.println("Usage: ATMSimulation propertiesFile");
        System.exit(0);
     }
     else
     {
      try
      {
         SimpleDataSource.init(args[0]);
      } 
      catch (Exception ex)
      {
        ex.printStackTrace();
        System.exit(0);
      }
     }
     
     JFrame frame = new ATM();
     frame.setTitle("First National Bank of Java");
     frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
     frame.pack();
     frame.show();
    }
   }   
